all:
	cd assets; npm run build-prod
	go generate
	go build 

dev:
	go-bindata -debug -prefix="assets/dist/" -pkg="data" -o="data/bindata.go" "assets/dist/..."
	go build 

run: dev
	./modbusutils
