package main

import (
	"fmt"
	"os"

	"bitbucket.org/automacprojects/modbusutils/cmd/modbus/cmd"
	"github.com/zserge/webview"
)

const port = 9237

//go:generate go run gen.go

func main() {
	if os.Getenv(EnvName) == "true" {
		cmd.Execute()
		return
	}

	if true {
		w := webview.New(webview.Settings{
			Title:     "Modbus Utils",
			URL:       fmt.Sprintf("http://localhost:%d/index.html", port),
			Debug:     true,
			Resizable: true,
		})
		defer w.Exit()
		go serve()
		w.Run()
	} else {
		serve()
	}

}
