// +build ignore

package main

import (
	"math/rand"
	"time"

	"bitbucket.org/automacprojects/data-polling/pollerd/simulator"
)

func main() {
	m := simulator.NewModbus()
	defer m.Close()
	m.Listen(10502)

	for i := 0; i < 1000; i++ {
		m.SetCoil(uint16(i), i%3 == 0)
	}

	for i := 0; i < 1000; i++ {
		m.SetRegisterFlags(uint16(i), uint32(i*i))
	}

	for i := 1000; i < 2000; i++ {
		m.SetRegisterFloat(uint16(i), float32(i)+rand.Float32())
	}

	for {
		time.Sleep(time.Second)
	}
}
