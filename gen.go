// +build ignore

package main

import (
	"path/filepath"
	"strings"

	bindata "github.com/zwzn/go-bindata"
)

func main() {
	cfg := &bindata.Config{
		Output:  "./data/bindata.go",
		Package: "data",
		Prefix:  "assets/dist/",
		Input: []bindata.InputConfig{
			parseInput("assets/dist/..."),
		},
	}
	bindata.Translate(cfg)
}

// from https://github.com/zwzn/go-bindata/blob/master/go-bindata/main.go
// parseRecursive determines whether the given path has a recrusive indicator and
// returns a new path with the recursive indicator chopped off if it does.
//
//  ex:
//      /path/to/foo/...    -> (/path/to/foo, true)
//      /path/to/bar        -> (/path/to/bar, false)
func parseInput(path string) bindata.InputConfig {
	if strings.HasSuffix(path, "/...") {
		return bindata.InputConfig{
			Path:      filepath.Clean(path[:len(path)-4]),
			Recursive: true,
		}
	} else {
		return bindata.InputConfig{
			Path:      filepath.Clean(path),
			Recursive: false,
		}
	}

}
