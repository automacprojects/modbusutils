package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/exec"

	"bitbucket.org/automacprojects/modbusutils/data"

	"github.com/davecgh/go-spew/spew"
)

var EnvName = "MODBUSUTIL_RUN_AS_CMD"

func serve() {
	http.HandleFunc("/run_command", runCMD)
	http.HandleFunc("/index.html", serveAsset("index.html"))
	http.HandleFunc("/main.js", serveAsset("main.js"))
	http.ListenAndServe("localhost:9237", nil)
}

func serveAsset(path string) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write(data.MustAsset(path))
	}
}

func runCMD(w http.ResponseWriter, r *http.Request) {
	args := []string{}
	err := json.NewDecoder(r.Body).Decode(&args)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return
	}
	os.Setenv(EnvName, "true")
	defer os.Setenv(EnvName, "false")

	spew.Dump(args)
	b, err := exec.Command(os.Args[0], args...).Output()
	if err != nil {
		w.WriteHeader(400)
	}
	w.Write(b)
}
