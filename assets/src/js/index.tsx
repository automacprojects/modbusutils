import Layout from 'js/components/layout'
import { h, render } from 'preact'
import 'promise-polyfill/src/polyfill'
import 'scss/app.scss'
import InputRegisters from './pages/input_registers'

render(<Layout>
    {/* <Coils /> */}
    <InputRegisters />
</Layout>, document.getElementById('app')!)
