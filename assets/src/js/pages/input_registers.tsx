import Form from 'js/components/form'
import Layout from 'js/components/layout'
import { ParseFlags, RunCommand } from 'js/modbus'
import { Component, h } from 'preact'
import 'scss/layout.scss'

interface State {
    data: string
    order: number[]
    type: 'float' | 'uint'
}

export default class InputRegisters extends Component<{}, State> {

    constructor(props: {}) {
        super(props)

        this.state = {
            data: '',
            order: [3, 2, 1, 0],
            type: 'float',
        }
    }

    public render() {
        return <Layout>
            <Form submit={this.run}>
                <input name='ip' placeholder='IP address' title='IP address' value='192.168.1.17:502' />
                <select name='func'>
                    <option value='input'>Input</option>
                    <option value='holding'>Holding</option>
                </select>
                <select name='dataType'>
                    <option value='float'>Float</option>
                    <option value='int'>Int</option>
                    <option value='raw'>Raw</option>
                </select>
                <input name='register' placeholder='register' title='register' type='number' min='1' value='1' />
                <input name='quantity' placeholder='quantity' title='quantity' type='number' min='1' value='64' />
                <input name='slaveID' placeholder='Slave ID' title='slave ID' type='number' min='0' value='17' />
                <button type='submit'>run</button>
            </Form>
            <pre>
                {colorize(this.state.data)}
            </pre>
        </Layout>
    }

    public run = async (data: any) => {
        RunCommand('read', data.func, ...ParseFlags({
            address: data.ip,
            startAddress: data.register,
            numCoils: data.quantity,
            slaveID: data.slaveID,
            dataType: data.dataType,
        }))
            .then(d => this.setState({ data: d }))
            .catch(err => this.setState({ data: err.toString() }))

    }

}

function colorize(str: string) {
    const out = []
    let i = 1
    for (const line of str.split('\n')) {
        const parts = line.split(':', 2)
        if (parts.length !== 2) {
            continue
        }
        out.push(
            <span key={i++} style={{ color: 'red' }}>{parts[0]}:</span>,
            <span key={i++} style={{ color: 'black' }}>{parts[1]}</span>,
            <br />,
        )
    }
    return out
}
