import { run } from './func'

interface Flags {
    address?: string
    byteOrder?: string
    dataType?: string
    numCoils?: number
    slaveID?: number
    startAddress?: number
}

export async function RunCommand(...args: string[]): Promise<string> {
    // return await run('modbus_cmd', args)
    return fetch('http://localhost:9237/run_command', {
        method: 'POST',
        body: JSON.stringify(args),
    }).then(async r => {
        if (!r.ok) {
            throw new Error(await r.text())
        }
        return r.text()
    })
}

export function ParseFlags(flags: Flags): string[] {
    const args: string[] = []
    if (flags.address !== undefined) {
        args.push('--address', flags.address)
    }
    if (flags.byteOrder !== undefined) {
        args.push('--byte-order', flags.byteOrder)
    }
    if (flags.dataType !== undefined) {
        args.push('--data-type', flags.dataType)
    }
    if (flags.numCoils !== undefined) {
        if (flags.numCoils < 1) {
            throw new Error('numCoils must be greater than 0')
        }
        args.push('--num-coils', flags.numCoils + '')
    }
    if (flags.slaveID !== undefined) {
        args.push('--slave-id', flags.slaveID + '')
    }
    if (flags.startAddress !== undefined) {
        if (flags.startAddress < 1) {
            throw new Error('startAddress must be greater than 0')
        }
        args.push('--start-address', flags.startAddress + '')
    }
    return args
}

function checkBytes(bytes: number[]): boolean {
    if (bytes.length !== 4) {
        return false
    }
    const hasNum = [false, false, false, false]

    for (const byte of bytes) {
        if (byte < 0 || byte > 3) {
            return false
        }
        if (hasNum[byte] === true) {
            return false
        }
        hasNum[byte] = true
    }

    return true
}
