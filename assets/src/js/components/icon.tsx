import { FunctionalComponent, h } from 'preact'
interface Props {
    name: string
}
const Icon: FunctionalComponent<Props> = props => <div class={props.name + ' icon'}><i /></div>

export default Icon
