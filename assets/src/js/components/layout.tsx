import { Component, h } from 'preact'
import 'scss/layout.scss'
import JSONpp from './jsonpp'

interface State {
    connected?: boolean
    info?: string
}

export default class Layout extends Component<{}, State> {

    private address: string = 'localhost:10502'

    constructor(props: {}) {
        super(props)

        this.state = {
            connected: false,
        }
    }

    public render() {
        return <div class='layout'>
            <nav class='nav' />
            <div class='content'>
                {this.props.children}
            </div>
        </div>
    }
}
