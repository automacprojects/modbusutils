import { FunctionalComponent, h } from 'preact'
const s = {
    bar: {
        position: 'fixed',
        width: '100%',
        top: '0',
        left: '0',
    },
}
const ToolBar: FunctionalComponent = props => <div style={s.bar}>
    <button>Connect</button>
</div>

export default ToolBar
