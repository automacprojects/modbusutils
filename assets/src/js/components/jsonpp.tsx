import { FunctionalComponent, h } from 'preact'

interface Props {
    data: any
}

const JSONpp: FunctionalComponent<Props> = props => <pre>
    {JSON.stringify(props.data, null, 2)}
</pre>

export default JSONpp
