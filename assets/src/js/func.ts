
declare const window: any
declare const provider: { run: (method: string, id: string, body: string) => void }

window.callbacks = {}

let id = 0

export function run(method: string, args: any): Promise<any> {
    return new Promise((resolve, reject) => {
        id++
        provider.run(method, id + '', JSON.stringify(args))
        method = method + id
        window.callbacks[method] = (data: any) => {
            resolve(data)

            delete window.callbacks[method]
            delete window.callbacks[`${method}_err`]
        }
        window.callbacks[`${method}_err`] = (err: any) => {
            reject(err)

            delete window.callbacks[method]
            delete window.callbacks[`${method}_err`]
        }
    })
}
