
// https://webpack.js.org/guides/production/

const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');


const paths = {
    DIST: path.resolve(__dirname, 'dist'),
    SRC: path.resolve(__dirname, 'src'),
    LIB: path.resolve(__dirname, 'src', 'lib'),
    JS: path.resolve(__dirname, 'src/js'),
    CSS: path.resolve(__dirname, 'src/scss'),
};

module.exports = (env, argv) => {
    const devMode = argv.mode !== 'production'
    return {
        mode: process.env.NODE_ENV,
        devtool: devMode ? 'source-map' : '',
        entry: {
            main: [path.join(paths.JS, 'index.tsx')],
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    loader: [
                        'ts-loader',
                        'tslint-loader',
                    ],
                },
                {
                    test: /\.s?css$/,
                    exclude: /node_modules/,
                    loader: [
                        'style-loader',
                        'css-loader',
                        'sass-loader',
                    ],
                },

            ],

        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js', '.scss', '.css'],
            modules: [
                "node_modules",
                paths.SRC,
                paths.LIB,
            ],
        },
        output: {
            path: paths.DIST,
            filename: '[name].js',
            chunkFilename: '[id].js',
        },
        plugins: [
            new HtmlWebpackPlugin({
                chunks: [
                    'main',
                ],
                template: path.join(paths.SRC, 'index.html'),
                filename: "index.html",
            }),
        ],
    }
};
