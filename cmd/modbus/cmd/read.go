// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/binary"
	"fmt"
	"math"
	"strings"

	"github.com/goburrow/modbus"
	"github.com/spf13/cobra"
)

// readCmd represents the read command
var readCmd = &cobra.Command{
	Use:   "read",
	Short: "Read from modbus with",
}

func init() {
	rootCmd.AddCommand(readCmd)

	readCmd.PersistentFlags().Uint16P("start-address", "s", 1, "the address to start reading from")
	readCmd.PersistentFlags().Uint16P("num-coils", "n", 2, "the number of coils to read")
	readCmd.PersistentFlags().StringP("byte-order", "b", "CDAB", "the order to treat the bytes")
	readCmd.PersistentFlags().StringP("data-type", "d", "float", "the type to print the data as, float, int, or raw")

	readCmd.PersistentFlags().Int8("slave-id", 0, "the slave id to use")
	readCmd.PersistentFlags().StringP("address", "a", "", "the ip address of the modbus slave (required)")
	readCmd.MarkPersistentFlagRequired("address")
}

func orderBytes(result []byte, byteOrder string) ([]byte, error) {
	// loads the byte order
	byteOrder = strings.ToUpper(byteOrder)

	if len(byteOrder) != 4 {
		return nil, fmt.Errorf("%s is an invalid byte-order, it must be 4 characters", byteOrder)
	}

	// makes sure each character from 'A' to 'A' + len(byteOrder) is in the string 1 time
	for i := 0; i < len(byteOrder); i++ {
		if strings.Count(byteOrder, string(i+'A')) != 1 {
			return nil, fmt.Errorf("byte-order must contain exactly 1 of 'A', 'B', 'C', and 'D'")
		}
	}

	// loads the ordered regester data from the regester data
	orderedResult := make([]byte, 4)
	for i, c := range byteOrder {
		orderedResult[i] = result[c-'A']
	}
	return orderedResult, nil
}

func formatResult(result []byte, byteOrder, dataType string) (string, error) {
	orderedResult, err := orderBytes(result, byteOrder)
	if err != nil {
		return "", err
	}

	dataType = strings.ToLower(dataType)
	switch dataType {
	case "float":
		f := math.Float32frombits(binary.BigEndian.Uint32(orderedResult))
		return fmt.Sprint(f), nil
	case "int":
		f := binary.BigEndian.Uint32(orderedResult)
		return fmt.Sprint(f), nil
	case "raw":
		return fmt.Sprintf("%02x %02x, %02x %02x", result[0], result[1], result[2], result[3]), nil

	default:
		return "", fmt.Errorf("'%s' is not a supported data-type", dataType)
	}
}

func printResults(results []byte, startAddress uint16, byteOrder, dataType string) error {
	for i := 0; i < len(results); i += 4 {
		s, err := formatResult(results[i:i+4], byteOrder, dataType)
		if err != nil {
			return err
		}
		address := startAddress + uint16(i)/2 + 1
		fmt.Printf("%3d, %3d: %s\n", address, address+1, s)
	}
	return nil
}

func getClient(cmd *cobra.Command) (modbus.Client, error) {
	ip, err := cmd.Flags().GetString("address")
	if err != nil {
		return nil, err
	}
	slaveID, err := cmd.Flags().GetInt8("slave-id")
	if err != nil {
		return nil, err
	}

	h := modbus.NewTCPClientHandler(ip)
	// h.Logger = log.New(os.Stdout, "", 0)
	h.SlaveId = byte(slaveID)
	return modbus.NewClient(h), nil
}

func doRead(function string) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		c, err := getClient(cmd)
		if err != nil {
			return err
		}
		numCoils, _ := cmd.Flags().GetUint16("num-coils")
		startAddress, _ := cmd.Flags().GetUint16("start-address")
		startAddress = startAddress - 1
		var results []byte
		if function == "holding" {
			results, err = c.ReadHoldingRegisters(startAddress, numCoils)
		} else if function == "input" {
			results, err = c.ReadInputRegisters(startAddress, numCoils)
		} else {
			return fmt.Errorf("unsupported function %s", function)
		}
		if err != nil {
			return err
		}

		byteOrder, _ := cmd.Flags().GetString("byte-order")
		dataType, _ := cmd.Flags().GetString("data-type")

		return printResults(results, startAddress, byteOrder, dataType)
	}
}
