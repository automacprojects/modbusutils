// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

// completionCmd represents the completion command
var completionCmd = &cobra.Command{
	Use:   "completion",
	Short: "Generates bash completion scripts",
	Long: `To load completion run

. <(modbus completion)

To configure your bash shell to load completions for each session add to your bashrc

# ~/.bashrc or ~/.profile
. <(modbus completion)

for zsh do the same but with in ~/.zshrc and with the --zsh flag

# ~/.zshrc or ~/.zprofile
. <(modbus completion --zsh)
`,
	Run: func(cmd *cobra.Command, args []string) {
		zsh, _ := cmd.Flags().GetBool("zsh")
		if zsh {
			rootCmd.GenZshCompletion(os.Stdout)
		} else {
			rootCmd.GenBashCompletion(os.Stdout)
		}
	},
}

func init() {
	// rootCmd.AddCommand(completionCmd)

	completionCmd.Flags().BoolP("zsh", "z", false, "generate zsh completion")
}
