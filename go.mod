module bitbucket.org/automacprojects/modbusutils

require (
	github.com/goburrow/modbus v0.1.0
	github.com/goburrow/serial v0.1.0 // indirect
	github.com/zserge/webview v0.0.0-20190123072648-16c93bcaeaeb
)
